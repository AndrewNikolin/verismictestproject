﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Management;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Principal;

namespace DesktopApp
{
    class ComputerInformation
    {
        public string Name { get; set; }
        public string Manufacturer { get; set; }
        public string LoggedInUsers { get; set; }

        public ComputerInformation()
        {
            Name = Environment.MachineName;
            var motherboardQuery = new ManagementObjectSearcher("SELECT Manufacturer FROM Win32_BaseBoard");
            foreach (var motherboard in motherboardQuery.Get())
            {
                Manufacturer = motherboard["Manufacturer"].ToString();
            }
            UpdateLoggedInUsers();
        }

        public void UpdateLoggedInUsers()
        {
            var userQuery = new ManagementObjectSearcher("SELECT * FROM Win32_UserProfile WHERE Loaded = True AND Special = False");
            var usersList = new List<string>();
            foreach (var user in userQuery.Get())
            {
                var username = new SecurityIdentifier(user["SID"].ToString()).Translate(typeof(NTAccount)).ToString();
                Debug.WriteLine(username);
                usersList.Add(username);
            }
            LoggedInUsers = string.Join(", ", usersList.ToArray());
        }
    }
}
