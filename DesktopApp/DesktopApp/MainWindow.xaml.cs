﻿using System.Diagnostics;
using System.Net;
using System.Timers;
using System.Windows;
using RestSharp;
using RestSharp.Serializers;

namespace DesktopApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        private const string Apiaddress = "http://localhost:5524";
        private ComputerInformation _pcInfo;
        private RestClient _client;
        public MainWindow()
        {
            InitializeComponent();
        }

        private void TimerOnElapsed(object sender, ElapsedEventArgs elapsedEventArgs)
        {
            _pcInfo.UpdateLoggedInUsers();
            SendData();
        }

        private void SendData()
        {
            var query = new RestRequest("api/info", Method.POST)
            {
                JsonSerializer = new JsonSerializer(),
                RequestFormat = RestSharp.DataFormat.Json
            };
            query.AddBody(_pcInfo);
            var response = _client.Execute(query);
            if (response.StatusCode != HttpStatusCode.Created)
            {
                MessageBox.Show("Error occured while sending data!");
            }
        }

        private void startButton_Click(object sender, RoutedEventArgs e)
        {
            _pcInfo = new ComputerInformation();
            _client = new RestClient(Apiaddress);
            SendData();
            var timer = new Timer(30 * 60 * 1000);
            timer.Elapsed += TimerOnElapsed;
        }
    }
}
