﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using APIServer.Models;

namespace APIServer.Controllers
{
    public class InfoController : ApiController
    {
        private APIServerContext db = new APIServerContext();

        // GET: api/Info
        public IQueryable<ComputerInformation> GetComputerInformations()
        {
            return db.ComputerInformations;
        }

        // GET: api/Info/5
        [ResponseType(typeof(ComputerInformation))]
        public IHttpActionResult GetComputerInformation(string id)
        {
            ComputerInformation computerInformation = db.ComputerInformations.Find(id);
            if (computerInformation == null)
            {
                return NotFound();
            }

            return Ok(computerInformation);
        }

        // POST: api/Info
        [ResponseType(typeof(ComputerInformation))]
        public IHttpActionResult PostComputerInformation(ComputerInformation computerInformation)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (db.ComputerInformations.Find(computerInformation.Name) != null)
            {
                var info = db.ComputerInformations.Find(computerInformation.Name);
                info.Manufacturer = computerInformation.Manufacturer;
                info.LoggedInUsers = computerInformation.LoggedInUsers;
            }
            else
                db.ComputerInformations.Add(computerInformation);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (ComputerInformationExists(computerInformation.Name))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = computerInformation.Name }, computerInformation);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ComputerInformationExists(string id)
        {
            return db.ComputerInformations.Count(e => e.Name == id) > 0;
        }
    }
}