namespace APIServer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ComputerInformations", "LoggedInUsers", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ComputerInformations", "LoggedInUsers");
        }
    }
}
